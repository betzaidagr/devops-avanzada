package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations dogsOp = new DogsOperations();
		String randomImage = new String();
		
		// Call getRandomDogImage operation and store the result on String
		randomImage = dogsOp.getRandomDogImage();
		
		// Assert true if the result string ends with '.jpg'
		assertTrue(randomImage.endsWith(".jpg"));
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations class
		DogsOperations dogsOp = new DogsOperations();
		
		// Call getBreedList operation and store the result on ArrayList
		ArrayList<String> breedList = new ArrayList<String>();
		breedList = dogsOp.getBreedList();
		
		// Assert true if the result ArrayList has size of more than 0
		assertTrue(breedList.size() > 0);
	}
}
